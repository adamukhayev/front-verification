import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocationService} from './location.service';
import {environment} from '../../environments/environment.prod';
import {environment1, environment2} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {

  city: any;

  constructor(private http: HttpClient,
              private location: LocationService) {
    this.city = location.getCitySettings();
  }

  createPerson(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment?.url}/api/add/send`, JSON.stringify(form), options).toPromise();
  }


  verify(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment1?.url}/api/email/check`, JSON.stringify(form), options).toPromise();

  }

  captcha(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment2?.url}/api/home/answer`, JSON.stringify(form), options).toPromise();
  }
}
