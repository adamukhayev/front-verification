import {Injectable} from '@angular/core';
import {SideMenu} from '../models/SideMenu';

@Injectable({
  providedIn: 'root'
})
export class SideMenuService {

  constructor() {
  }

  getSideMenuItems(): SideMenu[] {
    const items: SideMenu[] = [];
    items.push(this.getPanelSubmitRequest());
    return items;
  }

  getPanelSubmitRequest(): SideMenu {
    const sidemenu = new SideMenu();
    sidemenu.setParams({name: 'Подать заявку', icon: 'file', routing: 'submit', child: false});
    return sidemenu;
  }

}
